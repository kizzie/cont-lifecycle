require 'chef/provisioning/vagrant_driver'

with_driver 'vagrant'

#centos machine
centosoptions = {
    vagrant_options: {
      'vm.box' => 'ubuntu/trusty64',
      'vm.network' => ':forwarded_port, guest: 80, host: 8080'
    },
  }

  machine 'ubuntu-apache' do
    recipe 'myapache'
  #  recipe 'audit-cis::ubuntu1404-100'
    machine_options centosoptions
  end
