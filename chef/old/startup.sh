#gem install chef-provisioning chef-provisioning-vagrant
#export CHEF_DRIVER=vagrant
#export VAGRANT_DEFAULT_PROVIDER=virtualbox

#bring the boxes up and configure
chef-client -z --audit-mode enabled servers.rb provision.rb
