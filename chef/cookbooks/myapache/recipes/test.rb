
control_group 'webserver' do

  control 'apache working and listening on port 80' do
    it 'is installed' do
      expect(package('apache2')).to be_installed
    end
    it 'should be listening on port 80' do
      expect(port(80)).to be_listening
    end
  end

  control 'git should not be installed' do
    it 'is not installed' do
      expect(package('git')).to_not be_installed
    end
  end

  control 'port 8080' do
    it 'should not be open!' do
      expect(port(8080)).to_not be_listening
    end
  end
end
