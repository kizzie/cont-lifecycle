#
# Cookbook Name:: myapache
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

package 'install apache' do
  package_name 'apache2'
end

service 'apache2' do
  action :start
end

package 'install git' do
  package_name 'git'
#  action :remove
end

package 'sshd' do
  package_name 'ssh'
end

cookbook_file '/etc/ssh/sshd_config' do
  source 'sshd_config'
  owner 'root'
  group 'root'
  mode 00644
end
